package de.mdstv.bukkit.enoughmobs;

import org.bukkit.Chunk;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Main Class
 * @author Morph
 * @since 1.0
 */
public class EnoughMobs extends JavaPlugin implements Listener {
    public static final int MAX_ENTITIES = 5;

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
    }
    
    @EventHandler(ignoreCancelled = true)
    public void onEntitySpawn(CreatureSpawnEvent evt) {
        final Chunk    c        = evt.getEntity().getWorld().getChunkAt(evt.getLocation());
        final Entity[] entities = c.getEntities();
        
        if (entities.length >= MAX_ENTITIES) {
            evt.setCancelled(true);
        }
    }
}
